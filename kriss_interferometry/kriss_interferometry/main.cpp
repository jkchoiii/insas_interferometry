#include <iostream>

#include "stdafx.h"
#include "binaryio.hpp"
#include "mkl.h"
#include "Interferometric.h"

void fillComplex(std::vector<double>& a, std::vector<double>& b, std::vector<std::complex<double>>& cmplx);


int main()
{
	int data_count = 33333;
	int rows = 1760;

	//std::vector<double> real_up_data;
	//std::vector<double> imag_up_data;
	//std::vector<double> real_down_data;
	//std::vector<double> imag_down_data;

	//BinaryReader(real_up_data, "sas_data/up_real.bin");
	//BinaryReader(imag_up_data, "sas_data/up_imag.bin");
	//BinaryReader(real_down_data, "sas_data/down_real.bin");
	//BinaryReader(imag_down_data, "sas_data/down_imag.bin");
	


	//std::vector<std::complex<double>> cmplx_up_v;
	//std::vector<std::complex<double>> cmplx_down_v;
	//Eigen::MatrixXd up_realvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(real_up_data.data(), 1760, 33333);
	//Eigen::MatrixXd up_imagvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(imag_up_data.data(), 1760, 33333);
	//Eigen::MatrixXcd up_res = up_realvv + std::complex<double>(0, 1) * up_imagvv;

	//Eigen::MatrixXd down_realvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(real_down_data.data(), 1760, 33333);
	//Eigen::MatrixXd down_imagvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(imag_down_data.data(), 1760, 33333);
	//Eigen::MatrixXcd down_res = down_realvv + std::complex<double>(0, 1) * down_imagvv;


	int ping_length = rows / 32;
	double sonar_time_diff = 0.389723919056080;
	Interferometric InSAS(400e3, 7.78, 0.2, sonar_time_diff, data_count, ping_length, 1, 200e-6);
	
	SelectedLocation full_pos;
	full_pos.x_area << 0.0, 5.449018325805664;
	full_pos.y_area << 0.0, 30.0;
	InSAS.SetSASCoordinate(InSAS.full_map, full_pos, 1e-2, 1e-2);

	SelectedLocation sel_pos;
	sel_pos.x_area << 1.609129737609329e+03, 85.164723032069790;
	sel_pos.y_area << 3.669495391705069e+03, 3.974859447004608e+03;
	InSAS.ConvertCart2Range(sel_pos);
	InSAS.SetSASCoordinate(InSAS.up_map, sel_pos, 1e-2/4, 1e-2/4);
	InSAS.SetSASCoordinate(InSAS.down_map, sel_pos, 1e-2 / 4, 1e-2 / 4);

	int num_sensors = 32;
	int skip_ping = InSAS.GetSkipPing();
	//skip_ping += 4;

	
	//for (int i = 0; i < ping_length; i = i + skip_ping)
	//{ 

 //       std::cout << "up_data, index: " << i << ", skip_ping: " << skip_ping << std::endl;
 //       Eigen::MatrixXcd up_data = up_res.middleRows(i * num_sensors, num_sensors);
 //       InSAS.Process(i, InSAS.up_map, up_data);


 //       std::cout << "down_data, index: " << i << ", skip_ping: " << skip_ping << std::endl;
 //       Eigen::MatrixXcd down_data = down_res.middleRows(i * num_sensors, num_sensors);
 //       InSAS.Process(i, InSAS.down_map, down_data);

	//}



	//
	//Eigen::MatrixXd realvv = InSAS.up_map.image.raw_image.real();
	//Eigen::MatrixXd imagvv = InSAS.up_map.image.raw_image.imag();
	//std::vector<double> rr(realvv.size());
	//std::vector<double> ii(realvv.size());
	//std::copy(realvv.data(), realvv.data() + realvv.size(), rr.data());
	//std::copy(imagvv.data(), imagvv.data() + imagvv.size(), ii.data());
	//BinaryWriter(rr, "up_sas_real.bin");
	//BinaryWriter(ii, "up_sas_imag.bin");
	//realvv = InSAS.down_map.image.raw_image.real();
	//imagvv = InSAS.down_map.image.raw_image.imag();
	//rr.resize(realvv.size());
	//ii.resize(realvv.size());
	//std::copy(realvv.data(), realvv.data() + realvv.size(), rr.data());
	//std::copy(imagvv.data(), imagvv.data() + imagvv.size(), ii.data());
	//BinaryWriter(rr, "down_sas_real.bin");
	//BinaryWriter(ii, "down_sas_imag.bin");

	////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	/// bathymetry
    

	std::vector<double> up_real_sas;
    std::vector<double> up_imag_sas;
	std::vector<double> down_real_sas;
	std::vector<double> down_imag_sas;
	BinaryReader(up_real_sas, "./sas_data/up_real.bin");
	BinaryReader(up_imag_sas, "./sas_data/up_imag.bin");
	BinaryReader(down_real_sas, "./sas_data/down_real.bin");
	BinaryReader(down_imag_sas, "./sas_data/down_imag.bin");

	Eigen::MatrixXd up_realvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(up_real_sas.data(), 1497, 917);
	Eigen::MatrixXd up_imagvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(up_imag_sas.data(), 1497, 917);
	Eigen::MatrixXcd up_res = up_realvv + std::complex<double>(0, 1) * up_imagvv;

	Eigen::MatrixXd down_realvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(down_real_sas.data(), 1497, 917);
	Eigen::MatrixXd down_imagvv = Eigen::Map<Eigen::MatrixXd, Eigen::RowMajor>(down_imag_sas.data(), 1497, 917);
	Eigen::MatrixXcd down_res = down_realvv + std::complex<double>(0, 1) * down_imagvv;

	InSAS.up_map.image.raw_image = up_res;
	InSAS.down_map.image.raw_image = down_res;

	InSAS.RemoveInvalidValues(InSAS.up_map.image.raw_image);
	InSAS.RemoveInvalidValues(InSAS.down_map.image.raw_image);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	sel_pos.x_area << 9.497638483965015e+02, 5.395072886297378e+02;
	sel_pos.y_area << 1.431209677419355e+02, 1.388951612903226e+02;
	double new_dx = (InSAS.up_map.x_max - InSAS.up_map.x_min) / InSAS.up_map.image.raw_image.rows();
	double new_dy = (InSAS.up_map.y_max - InSAS.up_map.y_min) / InSAS.up_map.image.raw_image.cols();
	sel_pos.y_area = InSAS.up_map.y_min + sel_pos.y_area.array() * new_dy;
	sel_pos.x_area = InSAS.up_map.x_min + sel_pos.x_area.array() * new_dx;

	double cutoff = 0.2;
	InSAS.PreprocessImage(InSAS.up_map, cutoff);
	InSAS.PreprocessImage(InSAS.down_map, cutoff);

	InSAS.Bathymetry(InSAS.full_map, InSAS.up_map, InSAS.down_map, 
		InSAS.up_map.image.thrshd_image, InSAS.down_map.image.thrshd_image, sel_pos);

    return 0;
}

void fillComplex(std::vector<double>& a, std::vector<double>& b, std::vector<std::complex<double>>& cmplx) {

	cmplx.resize(a.size());
	std::transform(a.begin(), a.end(), b.begin(), cmplx.begin(), [](double da, double db) {
		return std::complex<double>(da, db);
	});
}
