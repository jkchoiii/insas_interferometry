#include "MatrixHelper.h"



void MatrixHelper::colon(double low, double step, double high, Eigen::VectorXd& dst)
{
	auto size = std::floor((high - low) / step) + 1;
	dst = Eigen::VectorXd::LinSpaced(size, low, low + step * (size - 1));
}

void MatrixHelper::colon(int low, int step, int high, Eigen::VectorXi& dst)
{
	auto size = std::floor((high - low) / step) + 1;
	dst = Eigen::VectorXi::LinSpaced(size, low, low + step * (size - 1));
}

void MatrixHelper::colon(float low, float step, float high, Eigen::VectorXf& dst)
{
	auto size = std::floor((high - low) / step) + 1;
	dst = Eigen::VectorXf::LinSpaced(size, low, low + step * (size - 1));
}

void MatrixHelper::getExactFFTPosition(Eigen::MatrixXcd& m, int dim)
{
	Eigen::FFT<double> fft;
	Eigen::VectorXcd trans;

	m = fftshift(m, dim).eval();

	if (dim == 1) {
		for (int i = 0; i < m.cols(); i++)
		{
			fft.fwd(trans, m.col(i));
			m.col(i) = trans;
		}
	}
	else if (dim == 2) {
		for (int i = 0; i < m.rows(); i++)
		{
			fft.fwd(trans, m.row(i));
			m.row(i) = trans;
		}
	}

	m = fftshift(m, dim).eval();

}

void MatrixHelper::getExactIFFTPosition(Eigen::MatrixXcd& m, int dim)
{
	Eigen::FFT<double> fft;
	Eigen::VectorXcd trans;

	m = ifftshift(m, dim).eval();

	if (dim == 1) {
		for (int i = 0; i < m.cols(); i++)
		{
			fft.inv(trans, m.col(i));
			m.col(i) = trans;
		}
	}
	else if (dim == 2) {
		for (int i = 0; i < m.rows(); i++)
		{
			fft.inv(trans, m.row(i));
			m.row(i) = trans;
		}
	}

	m = ifftshift(m, dim).eval();

}


