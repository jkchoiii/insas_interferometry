#pragma once
#define EIGEN_USE_MKL_ALL
#define EIGEN_MPL2_ONLY
#define EIGEN_FFTW_DEFAULT

#include <Eigen/Core>
#include <unsupported/Eigen/FFT>
#define _USE_MATH_DEFINES
#include <math.h>

#include "circ_shift.hpp"
#include "MatrixHelper.h"

