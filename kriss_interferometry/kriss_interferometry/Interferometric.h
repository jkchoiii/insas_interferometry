#pragma once
#include "stdafx.h"
#include "ipp.h"
#include "interp1.h"
typedef double f32;

struct ArrayParams
{
    f32 vt;
    int num_sensors;
    f32 dx;
    f32 D_t;
    f32 D_ap;
    Eigen::VectorXd XA;
    Eigen::VectorXd XT;
    f32 baseline;
};

struct PulseParams
{
    bool chirp;
    f32 pulse_length;
};

struct CoordinateParams
{
    f32 Xc;
    f32 Yc;
    f32 Zc;

    f32 ZT;
    f32 ZA;
    f32 YT;
    f32 YA;

    f32 odx;
    f32 ody;

    Eigen::VectorXd Dt;
};

struct InSASParams
{
    f32 cw;
    f32 fc;
    f32 fs;
    f32 depth;
    f32 delu;
    
    int data_count;
    int ping_length;
    int skip_ping;
    ArrayParams array;
    PulseParams pulse;
    CoordinateParams coord;

};

struct SASImage
{
    Eigen::MatrixXcd raw_image;
    Eigen::MatrixXcd thrshd_image;
    Eigen::MatrixXcd avg_image;
};

struct SASMap
{
    f32 ody;
    f32 odx;
    f32 x_min;
    f32 y_min;
    f32 x_max;
    f32 y_max;

    int trunc_idx;

    Eigen::MatrixXd XX;
    Eigen::MatrixXd YY;
    //Eigen::MatrixXcd image;
    SASImage image;

    Eigen::MatrixXd XTmat;
    Eigen::MatrixXd XAmat;
    Eigen::VectorXd ts;
};

struct SelectedLocation
{
    Eigen::Vector2d y_area;
    Eigen::Vector2d x_area;
};

struct ComplexInterp1
{
    mklInterp1 realv;
    mklInterp1 imagv;
};

class Interferometric
{
private:
    InSASParams m_params;
    MatrixHelper m_helper;
    //ComplexInterp1 m_interp1;
    mklInterp1* m_interp1;

    std::complex<double> jj;
    
    

    void SetParams(f32 fc, f32 depth, f32 vt, int data_count, int ping_length);
    
    void SetPulseParams(bool is_chirp, f32 f32);
    void SetCoordParams(f32 dt, int ping_length);
    void SetArrayParams(f32 vt);
    
    
public:
    SASMap full_map;
    SASMap up_map;
    SASMap down_map;
    int GetSkipPing();
    void ConvertCart2Range(SelectedLocation& sel_pos);
    void SetSASCoordinate(SASMap& map, SelectedLocation& sel_pos, f32 ody, f32 odx);
    void RemoveInvalidValues(Eigen::MatrixXcd& map);
    void GetDiff(Eigen::VectorXd& p, Eigen::VectorXd& dp);
    void PhaseUnwrap(Eigen::VectorXd& phase);
    void RowPhaseUnwrap(Eigen::MatrixXd& angles, Eigen::MatrixXd& unwrap);
    void Bathymetry(const SASMap& full_map, const SASMap& up_map, const SASMap& down_map, Eigen::MatrixXcd& up_image, Eigen::MatrixXcd& down_image, const SelectedLocation& sel_pos);
    Interferometric(f32 fc, f32 depth, f32 vt, f32 dt, int data_count, int ping_length, bool is_chirp, f32 pulse_length);
    void MovingAverage(f32 delu, f32 odx, Eigen::MatrixXcd& src_image, Eigen::MatrixXcd& dst_image);
    void PreprocessImage(SASMap& sas_map, double cutoff);
    //void ConCatComplexData(f32* realv_ptr, f32* imagv_ptr, Eigen::MatrixXcd& dst_cmplx, int trunc_idx);
    void ArrayPositionInit(Eigen::MatrixXd& XTmat, Eigen::MatrixXd& XAmat);
    void Process(int idx, SASMap& map, Eigen::MatrixXcd& cmplx_data);
    void IppAtan2(double* imag, double* real, double* dst, int size);
    void IppAtan2(Eigen::MatrixXd& matrix, Eigen::MatrixXd& tmp);
    void IppAtan2(Eigen::VectorXd& im, Eigen::VectorXd& re, Eigen::VectorXd& dst);
    void InterpolateComplexNumbers(Eigen::VectorXcd& data, Eigen::MatrixXd& dst, Eigen::VectorXd& ts, f32 c, Eigen::MatrixXcd& interp_mat);

    void mylininterp1f(Eigen::MatrixXd& x, Eigen::MatrixXd& y, Eigen::MatrixXd& x_query, Eigen::MatrixXd& y_query, double y_default, int x_size, int xq_size);

    void lininterp1f(double* x, double* y, double* x_query, double* y_query, double y_default, int x_size, int xq_size);
    void ProcessSyntheticApertureSonar(Eigen::MatrixXcd& data, Eigen::VectorXd& ts, Eigen::VectorXd& XT, Eigen::VectorXd& XA, SASMap& map);


};



