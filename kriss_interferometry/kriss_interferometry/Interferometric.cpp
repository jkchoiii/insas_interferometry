#include "Interferometric.h"
#include "binaryio.hpp"
#include <numeric>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "opencv2/core.hpp"
#include "opencv2/core/eigen.hpp"


#include <iostream>

void Interferometric::SetParams(f32 fc, f32 depth, f32 vt, int data_count, int ping_length)
{
    this->m_params.fc = fc;
    this->m_params.fs = 100e3f;
    this->m_params.depth = depth;
    this->m_params.ping_length = ping_length;
    this->m_params.delu = 25e-2f;
    
    this->m_params.cw = 1500.0f;
    int nskip = std::round(0.8f / vt);
    this->m_params.skip_ping = nskip < 1 ? 1 : nskip;
    this->m_params.data_count = data_count;

}
int Interferometric::GetSkipPing()
{
    return this->m_params.skip_ping;
}

void Interferometric::SetPulseParams(bool is_chirp, f32 pulse_length)
{
    this->m_params.pulse.chirp = is_chirp;
    this->m_params.pulse.pulse_length = pulse_length;
}

void Interferometric::SetArrayParams(f32 vt)
{
    this->m_params.array.vt = vt;
    this->m_params.array.num_sensors = 32;
    this->m_params.array.dx = 0.04;
    this->m_params.array.D_t = 0.22; // size of tx
    this->m_params.array.D_ap = 1.0 * this->m_params.array.dx;

    Eigen::VectorXd sensors;
    m_helper.colon(1.0f, 1.0f, double(m_params.array.num_sensors), sensors);
    this->m_params.array.XA = (sensors.array() - 1.0f) * this->m_params.array.dx;
    this->m_params.array.XT = (double(m_params.array.num_sensors - 1) / 2 * this->m_params.array.dx + 1.056) * Eigen::VectorXd::Ones(this->m_params.array.num_sensors);
    this->m_params.array.baseline = 18e-2;
        
}

void Interferometric::ConvertCart2Range(SelectedLocation& sel_pos)
{
    sel_pos.y_area = sel_pos.y_area.array() * (m_params.cw / 2.0f) / m_params.fs;
    sel_pos.x_area = (m_params.array.num_sensors - 1) * m_params.array.dx
                    + m_params.array.vt * m_params.coord.Dt(0) * ((sel_pos.x_area.array() / 32).floor() - 1);
}

void Interferometric::SetSASCoordinate(SASMap& map, SelectedLocation& sel_pos, f32 ody, f32 odx)
{
    Eigen::VectorXd y_area = sel_pos.y_area;

    map.x_min = sel_pos.x_area.minCoeff();
    map.x_max = sel_pos.x_area.maxCoeff();

    map.y_min = sel_pos.y_area.minCoeff();
    map.y_max = sel_pos.y_area.maxCoeff();

    map.odx = odx;
    map.ody = ody;

    Eigen::VectorXd x_range, y_range;
    m_helper.colon(map.x_min, odx, map.x_max, x_range);
    m_helper.colon(map.y_min, ody, map.y_max, y_range);
    
    map.XX = x_range.replicate(1, y_range.size());
    //map.XX.transpose();

    map.YY = y_range.transpose().replicate(x_range.size(), 1);
    map.image.raw_image = Eigen::MatrixXcd::Zero(x_range.size(), y_range.size());
    map.image.thrshd_image = Eigen::MatrixXcd::Zero(x_range.size(), y_range.size());
    map.image.avg_image = Eigen::MatrixXcd::Zero(x_range.size(), y_range.size());

    map.trunc_idx = 2 * std::round(sqrt((map.y_max* map.y_max) + pow(map.x_max / 2.0, 2)) / m_params.cw * m_params.fs);
    map.trunc_idx = map.trunc_idx > m_params.data_count ? m_params.data_count : map.trunc_idx;

    map.XTmat.resize(m_params.coord.Dt.size(), m_params.array.XT.size());
    map.XAmat.resize(m_params.coord.Dt.size(), m_params.array.XT.size());
    ArrayPositionInit(map.XTmat, map.XAmat);
    
    m_helper.colon(0, 1, map.trunc_idx - 1, map.ts);
    map.ts = map.ts.array() / m_params.fs;
}

void Interferometric::RemoveInvalidValues(Eigen::MatrixXcd& map)
{
    Eigen::MatrixXd real = map.real();
    Eigen::MatrixXd imag = map.imag();

    real = (real.array().isNaN()).select(0.0, real);
    imag = (imag.array().isNaN()).select(0.0, imag);

    map = real + jj * imag;
}

void Interferometric::GetDiff(Eigen::VectorXd& p, Eigen::VectorXd& dp)
{
    int dp_len = p.size() - 1;

    dp = p.tail(dp_len) - p.head(dp_len);

}

void Interferometric::PhaseUnwrap(Eigen::VectorXd& p)
{
    double cutoff = M_PI;

    Eigen::ArrayXd true_mask = Eigen::ArrayXd::Ones(p.size() - 1);
    Eigen::ArrayXd false_mask = Eigen::ArrayXd::Zero(p.size() - 1);

    Eigen::VectorXd dp;
    GetDiff(p, dp);
    
    Eigen::VectorXd dp_corr = dp.array() / (2.0 * M_PI);
    Eigen::VectorXd round_down = ((dp_corr.unaryExpr([](const double x) {return x >= 0 ? remainder(x, 1.0) : -1.0 + remainder(x, 1.0); }))
        .cwiseAbs().array() <= 0.5).select(true_mask, false_mask);

    dp_corr = (round_down.array() > 0.5).select(dp_corr.array().floor(), dp_corr);
    dp_corr = dp_corr.array().round();

    dp_corr = (dp.array().abs() < cutoff).select(0, dp_corr);

    Eigen::VectorXd cumsum(dp_corr.size());
    std::partial_sum(dp_corr.data(), dp_corr.data() + dp_corr.size(), cumsum.data());

    p.tail(p.size() - 1) = p.tail(p.size() - 1) - (2.0 * M_PI) * cumsum;

}

void Interferometric::RowPhaseUnwrap(Eigen::MatrixXd& angles, Eigen::MatrixXd& unwrap)
{
    for (int i = 0; i < unwrap.rows(); i++)
    {
        Eigen::VectorXd p = angles.row(i);
        PhaseUnwrap(p);
        unwrap.row(i) = p;
    }

}

void Interferometric::Bathymetry(const SASMap& full_map, const SASMap& up_map, const SASMap& down_map, Eigen::MatrixXcd& up_image, Eigen::MatrixXcd& down_image, const SelectedLocation& sel_pos)
{
    double y_start = sel_pos.y_area.minCoeff();

    double fc = m_params.fc;
    double fs = m_params.fs;
    double c = m_params.cw;
    double d = m_params.array.baseline;
    Eigen::VectorXd cross_range = full_map.YY.row(0);
    Eigen::VectorXd D = m_params.depth * Eigen::VectorXd::Ones(cross_range.size());
    Eigen::VectorXd full_SR = (cross_range.array().square() - D.array().square()).sqrt(); // slant_range
    full_SR = (full_SR.array().isNaN()).select(0, full_SR);


    Eigen::VectorXcd tmp = (jj * (2 * M_PI * fc * 2 * cross_range.array() / c)).exp();
    Eigen::VectorXd real = tmp.real();
    Eigen::VectorXd imag = tmp.imag();
    Eigen::VectorXd phase_CR(real.size());
    IppAtan2(imag, real, phase_CR);

    PhaseUnwrap(phase_CR);

    
    Eigen::VectorXd background = phase_CR.array() / (2.0 * M_PI * fc / c * d) * 1.1325 * full_SR.array();
    double dy = y_start - up_map.y_min;

    double num_queries = floor(dy / up_map.ody);
    Eigen::VectorXd query_pts = Eigen::VectorXd::LinSpaced(num_queries, up_map.y_min, y_start);
    Eigen::VectorXd background0(query_pts.size());

    lininterp1f(cross_range.data(), background.data(), query_pts.data(), background0.data(), 0.0, cross_range.size(), query_pts.size());

    Eigen::MatrixXd up_uwrap(up_image.rows(), up_image.cols());
    Eigen::MatrixXd down_uwrap(down_image.rows(), down_image.cols());
    Eigen::MatrixXd atan_angles(down_image.rows(), down_image.cols());


    Eigen::MatrixXd real_mat = up_image.real();
    Eigen::MatrixXd imag_mat = up_image.imag();

    IppAtan2(imag_mat.data(), real_mat.data(), atan_angles.data(), atan_angles.size());
    RowPhaseUnwrap(atan_angles, up_uwrap);



    real_mat = down_image.real();
    imag_mat = down_image.imag();

    IppAtan2(imag_mat.data(), real_mat.data(), atan_angles.data(), atan_angles.size());
    RowPhaseUnwrap(atan_angles, down_uwrap);

    Eigen::MatrixXd SR = (up_map.YY.array().square() - m_params.depth* m_params.depth).array().sqrt();
    Eigen::MatrixXd height_est0 = ((-1.0 * std::cos(M_PI * fc / (2.0 * fs))) * (up_uwrap + down_uwrap).array() / (4.0 * M_PI * fc / c * d) + 0.3214) * 1.1325 * SR.array();
    double max_height_coeff = height_est0.leftCols(num_queries).maxCoeff();
    double scale_factor = max_height_coeff / background0.mean();

    Eigen::MatrixXd height_est = m_params.depth - height_est0.array() * scale_factor;
    height_est = height_est.array() - height_est.leftCols(num_queries).mean();

    Eigen::MatrixXd realvv = height_est;
    std::vector<double> rr(height_est.size());

    std::copy(realvv.data(), realvv.data() + realvv.size(), rr.data());

    BinaryWriter(rr, "./sas_results/height.bin");
    

}

void Interferometric::SetCoordParams(f32 dt, int ping_length)
{
    this->m_params.coord.Xc = 0;
    this->m_params.coord.Yc = 0;
    this->m_params.coord.Zc = 0;
    this->m_params.coord.odx = 1e-2;
    this->m_params.coord.ody = 1e-2;
    this->m_params.coord.ZT = 0;
    this->m_params.coord.ZA = 0;
    this->m_params.coord.YT = 0;
    this->m_params.coord.YA = 0;
    this->m_params.coord.Dt = dt * Eigen::VectorXd::Ones(ping_length);
}


Interferometric::Interferometric(f32 fc, f32 depth, f32 vt, f32 dt, int data_count, int ping_length, bool is_chirp, f32 pulse_length)
{
    jj = std::complex<double>(0, 1);

    SetParams(fc, depth, vt, data_count, ping_length);
    SetArrayParams(vt);
    SetPulseParams(is_chirp, pulse_length);
    SetCoordParams(dt, ping_length);
    
}

void Interferometric::MovingAverage(f32 delu, f32 odx, Eigen::MatrixXcd& src_image, Eigen::MatrixXcd& dst_image)
{
    cv::Mat real_cv;
    cv::Mat imag_cv;
    cv::Mat kernel_cv;
    Eigen::MatrixXd real_eig = src_image.real();
    Eigen::MatrixXd imag_eig = src_image.imag();

    int mask_rows = round(delu / odx) + 1;
    Eigen::MatrixXd kernel_eig = Eigen::MatrixXd::Ones(mask_rows, 1);
    kernel_eig = kernel_eig.array() / kernel_eig.size();

    cv::eigen2cv(real_eig, real_cv);
    cv::eigen2cv(imag_eig, imag_cv);
    cv::eigen2cv(kernel_eig, kernel_cv);

    cv::Mat real_dst(real_cv.size(), real_cv.type());
    cv::Mat imag_dst(real_cv.size(), real_cv.type());

    cv::filter2D(real_cv, real_dst, -1, kernel_cv, cv::Point(0, 0), 0, cv::BORDER_CONSTANT);
    cv::cv2eigen(real_dst, real_eig);
    
    cv::filter2D(imag_cv, imag_dst, -1, kernel_cv, cv::Point(0, 0), 0, cv::BORDER_CONSTANT);
    cv::cv2eigen(imag_dst, imag_eig);

    dst_image = real_eig + jj * imag_eig;

}

//void Interferometric::ConCatComplexData(f32* realv_ptr, f32* imagv_ptr, Eigen::MatrixXcd& dst_cmplx, int trunc_idx)
//{
//       
//    dst_cmplx = realvv.leftCols(trunc_idx) + jj * imagvv.leftCols(trunc_idx);
//
//}

void Interferometric::PreprocessImage(SASMap& sas_map, double cutoff)
{
    //normalize max ~1.0
    sas_map.image.raw_image = sas_map.image.raw_image.array() / sas_map.image.raw_image.array().abs().maxCoeff();

    Eigen::MatrixXd real = sas_map.image.raw_image.real();
    Eigen::MatrixXd imag = sas_map.image.raw_image.imag();
    
    sas_map.image.thrshd_image = (sas_map.image.raw_image.array().abs() > cutoff).select(real, 0) + jj*(sas_map.image.raw_image.array().abs() > cutoff).select(imag, 0);
    
    MovingAverage(m_params.delu, sas_map.odx, sas_map.image.thrshd_image, sas_map.image.avg_image);


    //Eigen::MatrixXd realvv = sas_map.image.thrshd_image.real();
    //Eigen::MatrixXd imagvv = sas_map.image.thrshd_image.imag();
    //std::vector<double> rr(realvv.size());
    //std::vector<double> ii(realvv.size());
    //std::copy(realvv.data(), realvv.data() + realvv.size(), rr.data());
    //std::copy(imagvv.data(), imagvv.data() + imagvv.size(), ii.data());
    //BinaryWriter(rr, "./sas_results/up_thrshd_real.bin");
    //BinaryWriter(ii, "./sas_results/up_thrshd_imag.bin");

}

void Interferometric::ArrayPositionInit(Eigen::MatrixXd& XTmat, Eigen::MatrixXd& XAmat)
{
    Eigen::VectorXd XT = m_params.array.XT;
    Eigen::VectorXd XA = m_params.array.XA;

    for (int i = 0; i < m_params.coord.Dt.size(); i++)
    {
        XT = XT.array() + m_params.array.vt * m_params.coord.Dt(i);
        XTmat.row(i) = XT;
        XA = XA.array() + m_params.array.vt * m_params.coord.Dt(i);
        XAmat.row(i) = XA;
    }
}

void Interferometric::Process(int idx, SASMap& map, Eigen::MatrixXcd& cmplx_data)
{
    
    Eigen::MatrixXcd data;

    
    Eigen::VectorXd XT = Eigen::VectorXd::Zero(m_params.array.XT.size());
    Eigen::VectorXd XA = Eigen::VectorXd::Zero(m_params.array.XA.size());
    //for (int i = 0; i < m_params.ping_length; i = i + m_params.skip_ping)
    //{

        data = cmplx_data.leftCols(map.trunc_idx);
        XT = map.XTmat.row(idx);
        XA = map.XAmat.row(idx);
        
        ProcessSyntheticApertureSonar(data, map.ts, XT, XA, map);

    //}

    
}

void Interferometric::IppAtan2(double* imag, double* real, double* dst, int size)
{
    ippsAtan2_64f_A53(imag, real, dst, size);
}

void Interferometric::IppAtan2(Eigen::MatrixXd& srcTodst, Eigen::MatrixXd& src2)
{
    ippsAtan2_64f_A53(srcTodst.data(), src2.data(), srcTodst.data(), srcTodst.size());
}

void Interferometric::IppAtan2(Eigen::VectorXd& im, Eigen::VectorXd& re, Eigen::VectorXd& dst)
{
    ippsAtan2_64f_A53(im.data(), re.data(), dst.data(), re.size());
}

void Interferometric::InterpolateComplexNumbers(Eigen::VectorXcd& data, Eigen::MatrixXd& dst, Eigen::VectorXd& ts, f32 c, Eigen::MatrixXcd& interp_mat)
{
    Eigen::MatrixXd real = data.real();
    Eigen::MatrixXd imag = data.imag();
    
    Eigen::MatrixXd xq_mat = (dst.array() / c);
    xq_mat.transposeInPlace();

    Eigen::MatrixXd rtmp(interp_mat.cols(), interp_mat.rows());
    Eigen::MatrixXd itmp(interp_mat.cols(), interp_mat.rows());

    int rows = xq_mat.rows();
    int xqsize = xq_mat.rows();


#pragma omp parallel for
    for (int i = 0; i < dst.rows(); i++)
    {
        //lininterp1f(i * rows + rtmp.data(), ts.data(), real.data(), i * rows + xq_mat.data(), 0.0, ts.size(), xqsize);
        //lininterp1f(i * rows + itmp.data(), ts.data(), imag.data(), i * rows + xq_mat.data(), 0.0, ts.size(), xqsize);
        lininterp1f(ts.data(), real.data(), i * rows + xq_mat.data(), i * rows + rtmp.data(), 0.0, ts.size(), xqsize);
        lininterp1f(ts.data(), imag.data(), i * rows + xq_mat.data(), i * rows + itmp.data(), 0.0, ts.size(), xqsize);
    }
    
    
    interp_mat = (rtmp + jj * itmp).transpose();
}

void Interferometric::lininterp1f(double* x, double* y, double* x_query, double* y_query, double y_default, int x_size, int xq_size)
{
    
    for (int i = 0; i < xq_size; i++)
    {
        if ((x_query[i] < x[0]) || (x_query[i] > x[x_size - 1]))
            y_query[i] = y_default;
        else
            for (int j = 1; j < x_size; j++)
                if (x_query[i] <= x[j])
                {
                    y_query[i] = (x_query[i] - x[j - 1]) / (x[j] - x[j - 1]) * (y[j] - y[j - 1]) + y[j - 1];
                    break;
                }
    }

}

void Interferometric::mylininterp1f(Eigen::MatrixXd& x, Eigen::MatrixXd& y, Eigen::MatrixXd& x_query, Eigen::MatrixXd& y_query, double y_default, int x_size, int xq_size)
{
    for (int i = 0; i < xq_size; i++)
    {
        for (int j = 1; j < x_size; j++)
            if (x_query(i) <= x(j))
            {
                y_query(i) = (x_query(i) - x(j - 1)) / (x(j) - x(j - 1)) * (y(j) - y(j - 1)) + y(j - 1);
                break;
            }
    }

    y_query = ((x_query.array() < x(0)) || x_query.array() > x(x_size - 1)).select(y_default, y_query);
}



void Interferometric::ProcessSyntheticApertureSonar
(Eigen::MatrixXcd& data, Eigen::VectorXd& ts, Eigen::VectorXd& XT,   Eigen::VectorXd& XA, SASMap& map)
{

    f32 YA = 0;
    f32 YT = 0;
    f32 ZT = 0;
    f32 ZA = 0;
    
    f32 c = m_params.cw;
    f32 fc = m_params.fc;

    f32 k0 = 2 * M_PI * fc / c;

    f32 vtx = m_params.array.vt / c;    // vtx = vtx / c;
    f32 vty = 0 / c;                    // vty = vty / c;
    f32 v = sqrt(vtx*vtx + vty*vty);

    Eigen::MatrixXd r_mat(map.XX.rows(), map.XX.cols());
    Eigen::MatrixXd t_mat(map.XX.rows(), map.XX.cols());
    Eigen::MatrixXd dst(map.XX.rows(), map.XX.cols());
    Eigen::MatrixXd tmp(map.XX.rows(), map.XX.cols());
    Eigen::MatrixXcd pimage;

    Eigen::MatrixXd* dr;
    Eigen::MatrixXd* dt;
    Eigen::MatrixXd* theta_r;
    Eigen::MatrixXd* theta_t;

    Eigen::MatrixXcd interp_mat(dst.rows(), dst.cols());

    
    for (int i = 0; i < m_params.array.num_sensors; i++)
    {
        dr = &r_mat;
        dt = &t_mat;

        *dr = ((XA(i) - map.XX.array()).array().square() + (YA - map.YY.array()).array().square() + ZA*ZA).array().sqrt();
        *dt = ((XT(i) - map.XX.array()).array().square() + (YT - map.YY.array()).array().square() + ZT*ZT).array().sqrt();
        dst = (*dt).array() - (map.XX.array() - XA(i)) * vtx - (map.YY.array() - YA) * vty
            + (((map.XX.array() - XA(i)) * vtx + (map.YY.array() - YA) * vty - (*dt).array()).square()
            - (1 - v*v) * ((*dt).array().square() - (*dr).array().square())).array().sqrt();
        dst = dst.array() / (1 - v*v);

        //

        t_mat = map.XX.array() - XT(i);
        tmp = map.YY.array() - YT;
        IppAtan2(t_mat, tmp);
        theta_t = &t_mat;
        //

        r_mat = map.XX.array() - XA(i);
        tmp = map.YY.array() - YA;
        IppAtan2(r_mat, tmp);
        theta_r = &r_mat;

        // sinc
        t_mat = (k0 / (2.0 * M_PI) * m_params.array.D_t * (*theta_t).array().sin()).array().sin()
            / (k0 / (2.0 * M_PI) * m_params.array.D_t * (*theta_t).array().sin()).array();
        r_mat = (k0 / (2.0 * M_PI) * m_params.array.D_ap * (*theta_r).array().sin()).array().sin()
            / (k0 / (2.0 * M_PI) * m_params.array.D_ap * (*theta_r).array().sin()).array();

        // interp1
        
        Eigen::VectorXcd data_piece = data.row(i);
        
        InterpolateComplexNumbers(data_piece, dst, ts, c, interp_mat);
        
        
        pimage = (t_mat.array() * r_mat.array()).cwiseAbs() * interp_mat.array() * (jj * (2.0 * M_PI * fc * dst.array() / c).array()).exp();
        
        map.image.raw_image += pimage;
        
    }
    
    map.image.raw_image = map.image.raw_image.array() * map.YY.array() / (map.YY.col(0).maxCoeff());
    

}
