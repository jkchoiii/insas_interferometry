#pragma once
#include "mkl.h"
#include <stdlib.h>
#include <stdio.h>

class mklInterp1 {

private:
	DFTaskPtr task;                     // Data Fitting task descriptor
	MKL_INT nx;                         // number of break points
	MKL_INT ny;                         // number of functions
	MKL_INT nsite;

	MKL_INT xhint;                      // additional info about break points
	MKL_INT yhint;                      // additional info about function
	MKL_INT rhint;
	MKL_INT sitehint;
	MKL_INT scoeffhint;
	double* datahint;

	MKL_INT* dorder;
	MKL_INT ndorder;                    // size of array describing derivative
	MKL_INT stype, sorder;
	MKL_INT bc_type;
	MKL_INT ic_type;

	double* bc;
	double* ic;

	double* scoeff;
	long nscoeff;

	long data_size;

	void process(double* __restrict x, double* __restrict y, double* __restrict xq, double* __restrict results);
	void init(long data_size);
	void checkDfError(int num);

public:
	mklInterp1();
	~mklInterp1();
	void interpolate(long data_size, double* __restrict x, double* __restrict y, double* __restrict xq, double* __restrict results);

};