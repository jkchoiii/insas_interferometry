#pragma once
#include "stdafx.h"

class MatrixHelper {
public:
	void colon(double low, double step, double high, Eigen::VectorXd& dst);
	void colon(float low, float step, float high, Eigen::VectorXf& dst);
	void colon(int low, int step, int high, Eigen::VectorXi& dst);
	void getExactFFTPosition(Eigen::MatrixXcd& m, int dim);
	void getExactIFFTPosition(Eigen::MatrixXcd& m, int dim);

};