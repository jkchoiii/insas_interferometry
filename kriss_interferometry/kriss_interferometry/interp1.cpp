#include "interp1.h"

mklInterp1::mklInterp1()
{
    //this->sorder = DF_PP_LINEAR;
    //this->stype = DF_PP_DEFAULT;

    this->sorder = DF_PP_CUBIC;
    this->stype = DF_PP_BESSEL;

    this->xhint = DF_NO_HINT;
    this->yhint = DF_NO_HINT;
    this->rhint = DF_NO_HINT;
    this->sitehint = DF_NO_HINT;
    this->datahint = NULL;

    //this->bc_type = DF_NO_BC;
    this->bc_type = DF_BC_NOT_A_KNOT;
    this->ic_type = DF_NO_BC;

    this->bc = NULL;
    this->ic = NULL;

    constexpr MKL_INT NDORDER = 1;
    this->ndorder = NDORDER;

    this->dorder = new MKL_INT;
    dorder[0] = 1;

    this->scoeff = NULL;
    this->data_size = -1;
    this->nscoeff = -1;

}

mklInterp1::~mklInterp1()
{
    delete dorder;
}

void mklInterp1::interpolate(long data_size, double* __restrict x, double* __restrict y, double* __restrict xq, double* __restrict results)
{
    init(data_size);
    process(x, y, xq, results);

}

void mklInterp1::process(double* __restrict x, double* __restrict y, double* __restrict xq, double* __restrict results)
{
    int ny = 1;
    int errcode = 0;
    /***** Create Data Fitting task *****/
    errcode = dfdNewTask1D(&task, nx, x, xhint, ny, y, yhint);
    checkDfError(errcode);

    /***** Edit task parameters for linear spline construction *****/
    errcode = dfdEditPPSpline1D(task, sorder, stype, bc_type, bc,
        ic_type, ic, scoeff, scoeffhint);
    checkDfError(errcode);

    /***** Construct linear spline *****/
    errcode = dfdConstruct1D(task, DF_PP_SPLINE, DF_METHOD_STD);
    checkDfError(errcode);

    /***** Interpolate using linear method *****/
    errcode = dfdInterpolate1D(task, DF_INTERP, DF_METHOD_PP,
        nsite, xq, sitehint, ndorder,
        dorder, datahint, results, rhint, 0);
    checkDfError(errcode);

    /***** Delete Data Fitting task *****/
    errcode = dfDeleteTask(&task);
    checkDfError(errcode);
}

void mklInterp1::init(long data_size)
{
    this->nx = data_size;
    this->nsite = data_size;

    if (this->data_size == -1) {
        this->data_size = data_size;
        //nscoeff = (data_size - 1) * DF_PP_LINEAR;
        nscoeff = (data_size - 1) * DF_PP_CUBIC;

        scoeff = (double*)malloc(nscoeff * sizeof(double));
    }

    if (data_size != this->data_size) {
        this->data_size = data_size;
        //nscoeff = (data_size - 1) * DF_PP_LINEAR;
        nscoeff = (data_size - 1) * DF_PP_CUBIC;

        free(scoeff); // !!

        scoeff = (double*)malloc(nscoeff * sizeof(double));
    }


}

void mklInterp1::checkDfError(int num) {

    switch (num)
    {
    case DF_ERROR_NULL_TASK_DESCRIPTOR:
    {
        printf("Error: null task descriptor (code %d).\n", num);
        break;
    }
    case DF_ERROR_MEM_FAILURE:
    {
        printf("Error: memory allocation failure in DF functionality (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_NX:
    {
        printf("Error: the number of breakpoints is invalid (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_X:
    {
        printf("Error: the array which contains the breakpoints is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_X_HINT:
    {
        printf("Error: invalid flag describing structure of partition (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_NY:
    {
        printf("Error: invalid dimension of vector-valued function y (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_Y:
    {
        printf("Error: the array which contains function values is invalid (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_Y_HINT:
    {
        printf("Error: invalid flag describing structure of function y (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_SPLINE_ORDER:
    {
        printf("Error: invalid spline order (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_SPLINE_TYPE:
    {
        printf("Error: invalid type of the spline (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_IC_TYPE:
    {
        printf("Error: invalid type of internal conditions used in the spline construction (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_IC:
    {
        printf("Error: array of internal conditions for spline construction is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_BC_TYPE:
    {
        printf("Error: invalid type of boundary conditions used in the spline construction (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_BC:
    {
        printf("Error: array which presents boundary conditions for spline construction is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_PP_COEFF:
    {
        printf("Error: array of piece-wise polynomial spline coefficients is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_PP_COEFF_HINT:
    {
        printf("Error: invalid flag describing structure of the piece-wise polynomial spline coefficients (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_PERIODIC_VAL:
    {
        printf("Error: function values at the end points of the interpolation interval are not equal as required in periodic boundary conditions (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_DATA_ATTR:
    {
        printf("Error: invalid attribute of the pointer to be set or modified in Data Fitting task descriptor with EditIdxPtr editor (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_DATA_IDX:
    {
        printf("Error: index of pointer to be set or modified in Data Fitting task descriptor with EditIdxPtr editor is out of range (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_NSITE:
    {
        printf("Error: invalid number of interpolation sites (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_SITE:
    {
        printf("Error: array of interpolation sites is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_SITE_HINT:
    {
        printf("Error: invalid flag describing structure of interpolation sites (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_NDORDER:
    {
        printf("Error: invalid size of array that defines order of the derivatives to be computed at the interpolation sites (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_DORDER:
    {
        printf("Error: array defining derivative orders to be computed at interpolation sites is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_DATA_HINT:
    {
        printf("Error: invalid flag providing a-priori information about partition and/or interpolation sites (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_INTERP:
    {
        printf("Error: array of spline based interpolation results is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_INTERP_HINT:
    {
        printf("Error: invalid flag defining structure of spline based interpolation results (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_CELL_IDX:
    {
        printf("Error: array of indices of partition cells containing interpolation sites is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_NLIM:
    {
        printf("Error: invalid size of arrays containing integration limits (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_LLIM:
    {
        printf("Error: array of left integration limits is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_RLIM:
    {
        printf("Error: array of right integration limits is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_INTEGR:
    {
        printf("Error: array of spline based integration results is not defined (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_INTEGR_HINT:
    {
        printf("Error: invalid flag defining structure of spline based integration results (code %d).\n", num);
        break;
    }
    case DF_ERROR_BAD_LOOKUP_INTERP_SITE:
    {
        printf("Error: bad site provided for interpolation with look-up interpolator (code %d).\n", num);
        break;
    }
    case DF_ERROR_NULL_PTR:
    {
        printf("Error: bad pointer provided in DF function (code %d).\n", num);
        break;
    }
    default: break;
    }

    if (num < 0) {
        exit(1);
    }

}